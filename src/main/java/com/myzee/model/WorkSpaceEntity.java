package com.myzee.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ws_space")
public class WorkSpaceEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name="wid")
	private int wid;
	
	@Column(name = "ws_name")
	private String wsName;
	
	@Column(name = "ws_desc")
	private String wsDesc;
	
	@Column(name = "ws_location")
	private String wsLocation;
	
	public int getWid() {
		return wid;
	}
	public void setWid(int wid) {
		this.wid = wid;
	}
	public String getWsName() {
		return wsName;
	}
	public void setWsName(String wsName) {
		this.wsName = wsName;
	}
	public String getWsDesc() {
		return wsDesc;
	}
	public void setWsDesc(String wsDesc) {
		this.wsDesc = wsDesc;
	}
	public String getWsLocation() {
		return wsLocation;
	}
	public void setWsLocation(String wsLocation) {
		this.wsLocation = wsLocation;
	}
	
	
}
