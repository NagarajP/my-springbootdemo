package com.myzee.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.myzee.model.WorkSpaceEntity;

@Repository
public interface WSRepository extends JpaRepository<WorkSpaceEntity, Long>{
	
}
