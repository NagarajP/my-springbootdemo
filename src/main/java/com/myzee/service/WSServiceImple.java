package com.myzee.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myzee.model.WorkSpaceEntity;
import com.myzee.repository.WSRepository;

@Service
public class WSServiceImple implements WSService{

	@Autowired
	WSRepository repository;
	
	@Override
	public List<WorkSpaceEntity> getAllWS() {
		List<WorkSpaceEntity> list = new ArrayList<WorkSpaceEntity>();
		repository.findAll().forEach(e -> list.add(e));
		return list;
	}

	@Override
	public WorkSpaceEntity getWSById() {
		return null;
	}
	
}
