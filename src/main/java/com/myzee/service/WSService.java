package com.myzee.service;

import java.util.List;

import com.myzee.model.WorkSpaceEntity;

public interface WSService {
	public List<WorkSpaceEntity> getAllWS();
	public WorkSpaceEntity getWSById();
}
