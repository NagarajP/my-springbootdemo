package com.myzee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myzee.model.WorkSpaceEntity;
import com.myzee.service.WSService;

@RestController
@RequestMapping()
public class WSController {
	
	@Autowired (required = true)
	WSService wsservice;
	
	@GetMapping("getallws")
	public ResponseEntity<List<WorkSpaceEntity>> getAllWS() {
		System.out.println("getAllWS()");
		List<WorkSpaceEntity> listOfWS = wsservice.getAllWS();
		return new ResponseEntity<List<WorkSpaceEntity>>(listOfWS, HttpStatus.OK);
	}
}
