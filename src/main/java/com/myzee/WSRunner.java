package com.myzee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
//@EnableJpaRepositories("com.myzee.repository")
//@ComponentScan(basePackages = { "com.myzee.*" })
//@EntityScan("com.myzee.model")
public class WSRunner {

	public static void main(String[] args) {
		SpringApplication.run(WSRunner.class, args);
	}

}
